///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   01_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#include <stdio.h>
#include <string.h>


#include "addCats.h"
#include "catDatabase.h"
#include "config.h"

//#define DEBUG


int addCat(const char name[], const int gender, const int breed, const bool isFixed, const float weight, const int collarColor1, const int collarColor2, const unsigned long long license) {
   if (currentCats > MAX_CATS) {
      fprintf(stderr,"%s: Too many cats!\n", PROGRAM_NAME);
      return 1;
   }


   if (strlen(name) == 0) {
      fprintf(stderr, "%s: No cat name detected!\n", PROGRAM_NAME);
      return 1;
   }


   else if (strlen(name) > MAX_LENGTH) {
      fprintf(stderr, "%s: Cat name too long! Name must be 30 characters or less.\n", PROGRAM_NAME);
      return 1;
   }

   
   for (int i = 0; i <= currentCats; ++i) {
      if (cats[i].name == name) {
         fprintf(stderr, "%s: Name of cat must be unique!\n",PROGRAM_NAME);
         return 1;
      }
   }


   if (weight < 0) {
      fprintf(stderr, "%s: No cat detected! Weight must be greater than 0.\n", PROGRAM_NAME);
      return 1;
   }

   strcpy(cats[currentCats].name, name);
   cats[currentCats].gender = gender;
   cats[currentCats].breed = breed;
   cats[currentCats].isFixed = isFixed;
   cats[currentCats].weight = weight;
   cats[currentCats].collarColor1 = collarColor1;
   cats[currentCats].collarColor2 = collarColor2;
   cats[currentCats].license = license;

   currentCats += 1;
   printf("Cat successfully added!\n");
   return 0;

   #ifdef DEBUG
      printf("Current cats: %ld\n", currentCats);
      printf("Max length: %d\n", MAX_LENGTH);
      printf("Max cats: %d\n", MAX_CATS);
      printf("Name: %s\n", name);
      printf("Names of cats: %s\n", names[currentCats]);
      printf("Gender: %d\n", gender);
      printf("Genders of cats: %d\n", genders[currentCats]);
      printf("Breed: %d\n", breed);
      printf("Breeds of cats: %d\n", breeds[currentCats]);
      printf("Fixed: %d\n", isFixed);
      printf("Fixing of cats: %d\n", isFixedd[currentCats]);
      printf("Weight: %f\n", weight);
      printf("Weights of cats: %f\n", weights[currentCats]);
   #endif

}
