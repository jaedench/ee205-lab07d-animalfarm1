///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   01_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>

#include "catDatabase.h"
#include "deleteCats.h"
#include "config.h"

int deleteAllCats() {
   memset(cats, 0, sizeof(cats));
   currentCats = 0;
   printf("Deleted all cats\n");

   return 0;
}
