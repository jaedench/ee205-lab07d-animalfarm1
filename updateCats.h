///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   01_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern int updateCatName(const int index, const char newName[]);

extern int fixCat(const int index);

extern int updateCatWeight(const int index, const float newWeight);

extern int updateCatCollar1(const int index, const enum Color newColor1);

extern int updateCatCollar2(const int index, const enum Color newColor2);

extern int updateLicense(const int index, const unsigned long long newLicense);
