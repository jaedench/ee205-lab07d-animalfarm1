///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   01_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdbool.h> 
#include <stdio.h>

#include "catDatabase.h"

extern int addCat(const char name[], 
                  const int gender, 
                  const int breed, 
                  const bool isFixed, 
                  const float weight,
                  const int collarColor1,
                  const int collarColor2,
                  const unsigned long long license);
