///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   01_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "catDatabase.h"
#include "config.h"
#include "updateCats.h"

int updateCatName(const int index, const char newName[]) {
   if (index >= currentCats) {
      fprintf(stderr, "%s: No cat at index [%d]\n", PROGRAM_NAME, index);
      return 1;
   }


   if (index < 0) {
      fprintf(stderr, "%s: Index must be 0 or larger\n", PROGRAM_NAME);
      return 1;
   }


   if (strlen(newName) == 0) {
      fprintf(stderr, "%s: No empty name's allowed!\n", PROGRAM_NAME);
      return 1;
   }


   if (strlen(newName) > MAX_LENGTH) {
      fprintf(stderr, "%s: Cat's name is way too long! Must be 30 characters or less.\n", PROGRAM_NAME);
      return 1;
   }

   for (int i = 0; i < currentCats; i++) {
      if ((strcmp(cats[i].name, newName)) == 0) {
         fprintf(stderr, "%s: No duplicate names!\n", PROGRAM_NAME);
         return 1;
      }
   }


   strcpy(cats[index].name, newName);

   printf("Cat at index [%d] has been changed to [%s]!\n", index, newName);

   return 0;
}



int fixCat(const int index) {
    if (index >= currentCats) {
      fprintf(stderr, "%s: No cat at index [%d]\n", PROGRAM_NAME, index);
      return 1;
   }


   if (index < 0) {
      fprintf(stderr, "%s: Index must be 0 or larger\n", PROGRAM_NAME);
      return 1;
   }

  
   if (cats[index].isFixed == true) {
      fprintf(stderr, "%s: Cat is already fixed! Too late to go back now...\n", PROGRAM_NAME);
      return 1;
   }
   
   cats[index].isFixed = true;
   printf("Cat at index [%d] is fixed now!\n", index);
   return 0;
}


int updateCatWeight(const int index, const float newWeight) {
   if (index >= currentCats) {
      fprintf(stderr, "%s: No cat at index [%d]\n", PROGRAM_NAME, index);
      return 1;
   }


   if (index < 0) {
      fprintf(stderr, "%s: Index must be 0 or larger\n", PROGRAM_NAME);
      return 1;
   }


   if (newWeight <= 0) {
      fprintf(stderr, "%s: Is there a cat there? Weight must be greater than 0!\n", PROGRAM_NAME);
      return 1;
   }


   cats[index].weight = newWeight;
   printf("Cat weight at index [%d] has been changed to [%f]\n", index, newWeight);
   return 0;
}


int updateCatCollar1(const int index, const enum Color newColor1) {
   cats[index].collarColor1 = newColor1;
   return 0;
}


int updateCatCollar2(const int index, const enum Color newColor2) {
   cats[index].collarColor2 = newColor2;
   return 0;
}


int updateLicense(const int index, const unsigned long long newLicense) {
   cats[index].license = newLicense;
   return 0;
}

